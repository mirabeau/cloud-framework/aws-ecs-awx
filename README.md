AWS ECS AWX
===========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create AWX ECS services with CloudFormation.

AWS resources that will be created are:
 * ECR Repositories
   * AWX Web
   * AWX Task
   * Memcached
   * RabbitMQ
 * RDS PostgreSQL instance
 * ECS Services
   * AWX Web
   * AWX Task
   * Memcached
   * RabbitMQ
 * TaskDefinitions
   * AWX Web
   * AWX Task
   * Memcached
   * RabbitMQ
 * ELB Target group (if target ALB is given)
 * ELB Listener 80 Rule (if target ALB is given)
 * ELB Listener 443 Rule (if target ALB is given AND enable_443 is set to True - requires your ALB to have this listener)
 * Route53 record (if target ALB is given)

NOTE: This role has been tested for AWX version 1.0.8.0, so please use image_tag 1.0.8.0 or higher if you want something that works :)

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Pip 18.x or higher (Python 2.7)

Required python modules:
* boto
* boto3
* awscli

The role itself does not create an ECS cluster but takes an existing cluster name as a parameter for deployment.
Also note that while the Listener Rule(s) will be created it does not include any security group ingress rules since these are very customer specific.
You will need to add those yourself - i.e. in the `env-securitygroups-ingress` role.

Dependencies
------------
 * aws-lambda
 * aws-iam
 * aws-vpc
 * aws-vpc-lambda
 * aws-securitygroups
 * aws-hostedzone
 * aws-ecs-cluster
 * aws-ecs-alb
 * aws-rds-postgres

Role Variables
--------------
### Internal
```yaml
---
aws_ecs_awx_params:
  ecs_cluster_name: "{{ aws_ecs_cluster_params.cluster_name }}"  # keeping it for backwards compatability, ecs_cluster_stack_name should be used

aws_rds_postgres_name: "awx"

aws_rds_postgres_params:
  stack_name: "{{ environment_abbr }}-awx-data"

  database:
    name            : "awx"
    username        : "awx"
    endpoint_address:  # Optional, use if DB already exists

  rds:
    name         : "{{ environment_abbr }}-awx-db"
    username     : "root"
    storage      : "{{ aws_ecs_awx_params.rds.storage }}"
    version      : "{{ aws_ecs_awx_params.rds.version }}"
    instance_type: "{{ aws_ecs_awx_params.rds.instance_type }}"
    multi_az     : "{{ aws_ecs_awx_params.rds.multi_az }}"
    snapshot_name: "{{ aws_ecs_awx_params.rds.snapshot_name }}"
```

### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
-------------
```yaml
create_changeset   : True
debug              : False
cloudformation_tags: {}
tag_prefix         : "mcf"

aws_ecs_awx_params:
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"

  alb_role              : "ecs-alb"
  alb_scheme            : "internal"
  enable_443            : False  # NOTE: you need a 443 listener on your ALB to make this work
  ecs_cluster_stack_name: "{{ slicename | default(environment_abbr) }}-{{ aws_ecs_cluster_params.cluster_name }}"
  task:
    desiredcount: 1
    image       : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/awx/task"
    image_tag   : "latest"
  web:
    desiredcount: 1
    image       : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/awx/web"
    image_tag   : "latest"
  memcached:
    desiredcount: 1
    image       : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/awx/memcached"
    image_tag   : "latest"
  rabbitmq:
    desiredcount: 1
    image       : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/awx/rabbitmq"
    image_tag   : "latest"

  repositories:
    - task
    - web
    - memcached
    - rabbitmq

  rds:
    storage      : 50
    version      : "10.6"
    instance_type: "db.t2.small"
    multi_az     : False
    snapshot_name:  # Optional
```

Example Playbooks
-----------------
Build docker image and push to ECR
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    account_id      : "<your aws account id>"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
  tasks:
    - name: "awx | docker | build and push"
      include_role:
        name: aws-ecs-awx
        tasks_from: build-docker
```

Rollout the aws-ecs-awx files with defaults
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53:
      domainname: my.cloud
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
  pre_tasks:
    - name: Get latest AWS AMI's
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis
    - name: "Docker build and push"
      include_role:
        name: aws-ecs-awx
        tasks_from: build-docker
  roles:
    - aws-setup
    - aws-iam
    - aws-vpc
    - env-acl
    - aws-vpc-lambda
    - aws-securitygroups
    - aws-lambda
    - aws-ecs-cluster
    - aws-ecs-alb
    - aws-ecs-awx
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
