#!/bin/sh
# Usage: $0 entrypoint
# i.e. ./entrypoint.sh entrypoint
echo "[DEBUG] Called $# arguments: $@"

AZ=`curl -s --connect-timeout 1 http://169.254.169.254/latest/meta-data/placement/availability-zone`
if [ ! -z "$AZ" ]; then
	REGION=${AZ::-1}
else
	echo "[WARNING] No region could be detected, not running on AWS or hosed networking?"
fi

# Fetch SSM environment for this task
echo "[DEBUG] Loading aws-env with AWS_REGION[$AWS_REGION] and AWS_ENV_PATH[$AWS_ENV_PATH]...."
eval $(/usr/bin/aws-env)

# Fix database settings since SSM param names do not match what AWX expects.
if [ ! -z ${rds_host} ];
then
        echo "[DEBUG] Setting proper AWX values based on SSM stuff"
        mkdir /etc/tower/conf.d
				python /opt/awx_config.py --admin_user=${admin_user} \
			                       --admin_user_password=${admin_user_password} \
			                       --rds_host=${rds_host} \
			                       --rds_port=${rds_port} \
			                       --database_name=${database_name} \
			                       --database_user=${database_user} \
			                       --database_password=${database_password} \
			                       --rabbitmq_user=guest \
			                       --rabbitmq_password=guest \
			                       --rabbitmq_host=${rabbitmq_host} \
			                       --rabbitmq_port=5672 \
			                       --rabbitmq_vhost=awx \
			                       --memcached_host=${memcached_host}

				printf ${secret} >> /etc/tower/SECRET_KEY

else
        echo "[WARNING] Could not find rds_host from SSM - running locally? Not overriding any stuff, have fun..."
fi
source /etc/tower/conf.d/environment.sh

# Call docker-entrypoint
echo "[DEBUG] Starting requested entrypoint: $@"
exec "$@"
