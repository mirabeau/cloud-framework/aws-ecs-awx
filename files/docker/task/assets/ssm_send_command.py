#!/usr/bin/python
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'community'}


DOCUMENTATION = '''
---
module: ssm_send_command
short_description: Send a SSM command
description:
  - This module sends AWS SSM Commands.
version_added: "2.6"
extends_documentation_fragment:
  - aws
author: "Rob Reus <rreus@mirabeau.nl>"
requirements:
  - python >= 2.6
  - boto3
notes:
options:
  document_name
    description:
      - Required. The name of the Systems Manager document to execute. 
        This can be a public document or a custom document.
  document_version:
    description:
      - The  SSM  document  version  to  use in the request. You can specify
        $DEFAULT, $LATEST, or a specific version number.
  document_hash:
    description:
      - The Sha256 hash created by the system when the document was created.
  instance_ids:
    description:
      - The instance IDs where the command should execute. You can specify a
        maximum of 50 IDs. If you prefer not  to  list  individual  instance
        IDs, you can instead send commands to a fleet of instances using the
        targets parameter, which accepts  EC2  tags.
  targets:
    description:
      - An array of search criteria that targets instances  using
        a Key,Value combination that you specify. targets is required if you
        don't provide one or more instance IDs in the call.
  timeout_seconds:
    description:
      - If this time is reached and the command has not already started exe-
        cuting, it will not run.
  comment:
    description:
      - User-specified information  about  the  command,  such  as  a  brief
        description of what the command should do.
  parameters:
    description:
      - The required and optional parameters specified in the document being
        executed.
  output_s3_bucket_name:
    description:
      - The name of the S3 bucket where command execution  responses  should
        be stored.
  output_s3_key_prefix:
    description:
      - The  directory  structure  within  the S3 bucket where the responses
        should be stored.
  max_concurrency:
    description:
      - The maximum number of instances that are allowed to  exe-
        cute  the command at the same time. You can specify a number such as
        10 or a percentage such as 10%. The default value is  50.
  max_errors:
    description:
      - The maximum number of errors allowed without  the  command  failing.
        When  the command fails one more time beyond the value of MaxErrors,
        the systems stops sending the command to additional targets. You can
        specify a number like 10 or a percentage like 10%. The default value
        is 0.
  service_role_arn:
    description:
      - The IAM role that Systems Manager uses to send notifications.
  notification_config:
    description:
      - Configurations for sending notifications.
  cloud_watch_output_config:
    description:
      - Enables Systems Manager to send Run Command output to Amazon Cloud-
        Watch Logs.
  wait:
    description:
      - Wait for the command to finish executing
'''

EXAMPLES = '''
- ssm_send_command:
    document_name: AWS-RunShellScript
    parameters:
      commands:
        - "/etc/init.d/httpd reload"
        - "/etc/init.d/php-fpm restart"
      timeout: 60
      targets:
      - Key: tag:Environment
        Values:
          - Production
      - Key: tag:Role
        Values:
          - Webserver
  register: response

- ssm_send_command:
    document_name: AWS-RunShellScript
    parameters:
      commands:
        - "/etc/init.d/httpd reload"
        - "/etc/init.d/php-fpm restart"
      timeout: 60
      instance_ids:
        - i-ad4f8b6dc3f95324ac
        - i-cb8a88ebd9682d8d69
    output_s3_bucket_name: my-s3-bucket
    output_s3_key_prefix: ssm-commands
    wait: True
  register: response
'''

RETURN = '''
command_id:
    description: A unique identifier for this command.
    returned: success
    type: string
    sample: 0d4fc863-2154-4e46-990e-d6a952469e91
command_result:
    description: The result of the command (Success, Failed etc)
    returned: success when wait is true
    type: string
    sample: Success
'''

import traceback
from time import sleep

try:
    import botocore
    HAS_BOTO3 = True
except ImportError:
    HAS_BOTO3 = False

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.ec2 import boto3_conn, ec2_argument_spec, get_aws_connection_info
from ansible.module_utils._text import to_native


def main():
    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
        document_name=dict(required=True),
        document_version=dict(default="$DEFAULT"),
        document_hash=dict(default=None, required=False),
        instance_ids=dict(default=None, required=False, type='list'),
        targets=dict(default=None, required=False, type='list'),
        timeout_seconds=dict(default=None, required=False, type='int'),
        comment=dict(default=None, required=False),
        parameters=dict(default=None, required=False, type='dict'),
        output_s3_bucket_name=dict(default=None, required=False),
        output_s3_key_prefix=dict(default=None, required=False),
        max_concurrency=dict(default=None, required=False),
        max_errors=dict(default=None, required=False),
        service_role_arn=dict(default=None, required=False),
        notification_config=dict(default=None, required=False),
        cloud_watch_output_config=dict(default=None, required=False),
        wait=dict(default=None, required=False, type='bool'),
    ))
    module = AnsibleModule(
        argument_spec=argument_spec,
        supports_check_mode=True,
        mutually_exclusive=[
            ['instance_ids', 'targets'],
        ]
    )

    if not HAS_BOTO3:
        module.fail_json(msg='boto3 required for this module')

    document_name = module.params.get('document_name')
    document_version = module.params.get('document_version')
    document_hash = module.params.get('document_hash')
    instance_ids = module.params.get('instance_ids')
    targets = module.params.get('targets')
    timeout_seconds = module.params.get('timeout_seconds')
    comment = module.params.get('comment')
    parameters = module.params.get('parameters')
    output_s3_bucket_name = module.params.get('output_s3_bucket_name')
    output_s3_key_prefix = module.params.get('output_s3_key_prefix')
    max_concurrency = module.params.get('max_concurrency')
    max_errors = module.params.get('max_errors')
    service_role_arn = module.params.get('service_role_arn')
    notification_config = module.params.get('notification_config')
    cloud_watch_output_config = module.params.get('cloud_watch_output_config')
    wait = module.params.get('wait')

    if not (instance_ids or targets):
        module.fail_json(msg="Must provide either instance_ids or targets.")

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=HAS_BOTO3)
    if not region:
        module.fail_json(msg="The AWS region must be specified as an "
                         "environment variable or in the AWS credentials "
                         "profile.")

    try:
        client = boto3_conn(module, conn_type='client', resource='ssm',
                            region=region, endpoint=ec2_url, **aws_connect_kwargs)
    except (botocore.exceptions.ClientError, botocore.exceptions.ValidationError) as e:
        module.fail_json(msg="Failure connecting boto3 to AWS: %s" % to_native(e), exception=traceback.format_exc())

    invoke_params = dict()

    invoke_params['DocumentName'] = document_name
    if instance_ids:
        invoke_params['InstanceIds'] = instance_ids
    if targets:
        invoke_params['Targets'] = targets
    if document_version:
        invoke_params['DocumentVersion'] = document_version
    if document_hash:
        invoke_params['DocumentHash'] = document_hash
        invoke_params['DocumentHashType'] = 'Sha256',
    if timeout_seconds:
        invoke_params['TimeoutSeconds'] = timeout_seconds
    if comment:
        invoke_params['Comment'] = comment
    if parameters:
        invoke_params['Parameters'] = parameters
    if output_s3_bucket_name:
        invoke_params['OutputS3BucketName'] = output_s3_bucket_name
        if output_s3_key_prefix:
            invoke_params['OutputS3KeyPrefix'] = output_s3_key_prefix
    if max_concurrency:
        invoke_params['MaxConcurrency'] = max_concurrency
    if max_errors:
        invoke_params['MaxErrors'] = max_errors
    if service_role_arn:
        invoke_params['ServiceRoleArn'] = service_role_arn
    if notification_config:
        invoke_params['NotificationConfig'] = notification_config
    if cloud_watch_output_config:
        invoke_params['CloudWatchOutputConfig'] = cloud_watch_output_config

    try:
        response = client.send_command(**invoke_params)
    except botocore.exceptions.ClientError as ce:
        if ce.response['Error']['Code'] == 'InvalidDocument':
            module.fail_json(msg="Could not find document to execute. Make sure "
                             "the document_name is correct and your profile has "
                             "permissions to execute this document.",
                             exception=traceback.format_exc())
        if ce.response['Error']['Code'] == 'InvalidInstanceId':
            module.fail_json(msg="Invalid InstanceId, make sure it exists.",
                             exception=traceback.format_exc())
        module.fail_json(msg="Client-side error when sending command, check inputs and specific error",
                         exception=traceback.format_exc())
    except botocore.exceptions.ParamValidationError as ve:
        module.fail_json(msg="Parameters to `send_command` failed to validate",
                         exception=traceback.format_exc(ve))
    except Exception as e:
        module.fail_json(msg="Unexpected failure while invoking send_command",
                         exception=traceback.format_exc())

    try:
        command_id = response['Command']['CommandId']
    except KeyError:
        module.fail_json(msg="Did not receive CommandId back from send_command. "
                         "Check the AWS logs for more information",
                         exception=traceback.format_exc())

    results = dict()

    if wait is True:
        done = False
        while not done:
            try:
                list_commands = client.list_commands(CommandId=command_id)
            except botocore.exceptions.ParamValidationError as ve:
                module.fail_json(msg="Parameters to `send_command` failed to validate",
                                 exception=traceback.format_exc(ve))
            except botocore.exceptions.ClientError as ce:
                if ce.response['Error']['Code'] == 'ValidationException':
                    module.fail_json(msg="Unable to validate CommandId.",
                                     exception=traceback.format_exc())
            except Exception as e:
                module.fail_json(msg="Unexpected failure while invoking list_commands",
                                 exception=traceback.format_exc())

            if len(list_commands['Commands']) is 0:
                module.fail_json(msg="Unable to find command with CommandId: {}".format(command_id))
            else:
                command = list_commands['Commands'][0]

            if command['Status'] == 'Success':
                results['status'] = command['Status']
                results['target_count'] = command['TargetCount']
                results['completed_count'] = command['CompletedCount']
                results['error_count'] = command['ErrorCount']
                results['timeout_count'] = command['DeliveryTimedOutCount']
                done = True
            elif command['Status'] in ['Failed', 'TimedOut', 'Cancelled']:
                module.fail_json(msg="CommandId {} {}".format(command_id, command['Status']))
                done = True
            else:
                sleep(2)
    else:
        results['command_id'] = response['Command']['CommandId']

    module.exit_json(changed=True, result=results)


if __name__ == '__main__':
    main()
