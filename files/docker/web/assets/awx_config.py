#!/usr/bin/env python

import argparse
from jinja2 import Environment, FileSystemLoader

def parse_args():
    parser = argparse.ArgumentParser("AWX config file helper")
    parser.add_argument("--admin_user", help="AWX admin user")
    parser.add_argument("--admin_user_password", help="AWX admin password")
    parser.add_argument("--rds_host", help="RDS host")
    parser.add_argument("--rds_port", help="RDS port")
    parser.add_argument("--database_name", help="DB name")
    parser.add_argument("--database_user", help="DB user")
    parser.add_argument("--database_password", help="DB password")
    parser.add_argument("--rabbitmq_user", help="RabbitMQ user")
    parser.add_argument("--rabbitmq_password", help="RabbitMQ password")
    parser.add_argument("--rabbitmq_host", help="RabbitMQ host")
    parser.add_argument("--rabbitmq_port", help="RabbitMQ port")
    parser.add_argument("--rabbitmq_vhost", help="RabbitMQ vhost")
    parser.add_argument("--memcached_host", help="Memcached Hostname")
    args = parser.parse_args()
    return args

def jinja2_load():
    file_loader = FileSystemLoader("/opt/templates/")
    env = Environment(loader=file_loader)
    return env

def create_environment_file(args, env):
    template = env.get_template('environment.sh.j2')
    output = template.render(
             pg_username=args.database_user,
             pg_database=args.database_name,
             pg_hostname=args.rds_host,
             pg_port=args.rds_port,
             pg_password=args.database_password,
             memcached_hostname=args.memcached_host,
             rabbitmq_hostname=args.rabbitmq_host,
             admin_user=args.admin_user,
             admin_password=args.admin_user_password)

    write_to_file(output, '/etc/tower/conf.d/environment.sh')

def create_credentials_file(args, env):
    template = env.get_template('credentials.py.j2')
    output = template.render(
            pg_username=args.database_user,
            pg_database=args.database_name,
            pg_hostname=args.rds_host,
            pg_port=args.rds_port,
            pg_password=args.database_password,
            rabbitmq_hostname=args.rabbitmq_host,
            rabbitmq_user=args.rabbitmq_user,
            rabbitmq_password=args.rabbitmq_password,
            rabbitmq_port=args.rabbitmq_port,
            rabbitmq_vhost=args.rabbitmq_vhost)

    write_to_file(output, '/etc/tower/conf.d/credentials.py')

def write_to_file(output, destination):
    with open(destination, 'wb') as fh:
        fh.write(output)

def main():
    args = parse_args()
    env = jinja2_load()
    create_environment_file(args, env)
    create_credentials_file(args, env)

main()
